#include "MessageSender.h"
vector<string> users_vec;
queue<string> messages_queue;
condition_variable cv;
bool ready = false;
mutex m;
ofstream myfile;
ifstream myReadFile;

void MessageSender::printMenu()
{
	int option = 0;

	do {
		cout << "1. Signin" << endl;
		cout << "2. Signout" << endl;
		cout << "3. Connected Users" << endl;
		cout << "4. exit" << endl;
		cin >> option;

		switch (option)
		{
		case 1:
			Signin();
			break;
		case 2:
			Signout();
			break;
		case 3:
			ConnectedUsers();
			break;
		case 4 :
			myfile.close();
			myReadFile.close();
			break;
		default:
			cout << "wrong input, try again" << endl;
			break;
		}
	} while (option != EXIT);
}

void MessageSender::Signin()
{
	string user_name;
	bool exist = false;
	int i = 0;

	cout << "Enter user name" << endl;
	cin >> user_name;
	for (i = 0; i < users_vec.size(); i++)
	{
		if (users_vec[i] == user_name)
		{
			exist = true;
		}
	}
	if (!exist)
	{
		users_vec.push_back(user_name);
	}
	else {
		cout << "user name already exists" << endl;
	}
}

void MessageSender::Signout()
{
	string user_name;
	bool exist = false;
	int i = 0;
	int found = 0;

	cout << "Enter user name" << endl;
	cin >> user_name;
	for (i = 0; i < users_vec.size(); i++)
	{
		if (users_vec[i] == user_name)
		{
			exist = true;
			found = i + 1;
		}
	}
	if (exist)
	{
		users_vec.erase(users_vec.begin(), users_vec.begin() + found);
	}
	else
	{
		cout << "user name isn't exist" << endl;
	}
}

void MessageSender::ConnectedUsers()
{
	int i = 0;

	for (i = 0; i < users_vec.size(); i++)
	{
		cout << users_vec[i] << endl;
	}
}

// This function pushes to the messages queue all the massages in data.txt
void messages()
{
	string line;
	myReadFile.open("c:/tmp/data.txt");
	while (true)
	{
		if (myReadFile.is_open())
			m.lock();
		while (getline(myReadFile, line))
		{
			messages_queue.push(line);
		}
		m.unlock();
		cv.notify_one();
		remove("C:\semC\EX12\data.txt");
		this_thread::sleep_for(chrono::seconds(60));
	}
}

// This function writes to output.txt the messages, with the user name
void send_messages()
{ 
	myfile.open("C:/tmp/output.txt");
	while (true)
	{
		if (myfile.is_open())
		{
			if (!messages_queue.empty())
			{
				for (int i = 0; i < users_vec.size(); i++)
				{
					while (!messages_queue.empty()) {
						m.lock();
						myfile << users_vec[i] << ": " << messages_queue.front() << endl;
						messages_queue.push(messages_queue.front());
						messages_queue.pop();
						m.unlock();
					}

				}
			}
			else {
				unique_lock<std::mutex> lk(m);
				cv.wait(lk, [] {return ready; });
			}
		}
	}
}