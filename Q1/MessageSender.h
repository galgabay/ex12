#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <vector>
#include <thread>
#include <queue>
#include <mutex>
#include <condition_variable>

#define EXIT 4
using namespace std;

class MessageSender
{
public:
	void printMenu();
	void Signin();
	void Signout();
	void ConnectedUsers();
};

void messages();
void send_messages();
