#include "threads.h"
using namespace std;
mutex mtx;

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = 0;
	int j = 0;
	bool is_prime = true;

	for (i = begin; i <= end; i++)
	{
		mtx.lock();
		is_prime = true;
		for (j = 2; j <= sqrt(i); j++)
		{
			if ((i % j) == 0)
			{
				is_prime = false;
			}
		}
		if (is_prime)
		{
			file << i << endl;
		}
		mtx.unlock();
	}
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	int i = 0;
	int range = (end - begin) / N;
	int start = begin;
	vector<thread> myThreads;
	ofstream ofile(filePath);

	auto startTime = chrono::high_resolution_clock::now();
	if (ofile.is_open())
	{

		for (i = 0; i < N; i++)
		{
			myThreads.push_back(thread(writePrimesToFile, start, start + range - 1, ref(ofile)));
			start += range;
		}
		for (thread& th : myThreads)
		{
			th.join();
		}

		ofile.close();
	}

	auto elapsedTime = chrono::high_resolution_clock::now() - startTime;
	long long microseconds = chrono::duration_cast<chrono::microseconds>(elapsedTime).count();
	double seconds = microseconds / 1000000.0;
	cout << "Function writePrimesToFile took " << seconds << " seconds" << endl;
}